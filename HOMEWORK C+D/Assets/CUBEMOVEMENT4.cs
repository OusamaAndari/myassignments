﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUBEMOVEMENT4 : MonoBehaviour {
    [SerializeField] float m_movementspeed = 100;
    Rigidbody m_myRigidbody;

    // Use this for initialization
    void Start()
    {
        ///gets the rigid body component from the current game object
        m_myRigidbody = gameObject.GetComponent<Rigidbody>();

        if (m_myRigidbody == null)
        {
            Debug.Log("NotificationServices Rigidbody found in PhysicalMovementController");

        }
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        /// we creating direction vector for the force
        Vector3 movementForce = new Vector3();

        movementForce.x = horizontalMovement * m_movementspeed * Time.deltaTime;
        movementForce.z = verticalMovement * m_movementspeed * Time.deltaTime;

        ///and add tp force as an impulse to the rigid body
        m_myRigidbody.AddForce(movementForce, ForceMode.Impulse);


    }
}
