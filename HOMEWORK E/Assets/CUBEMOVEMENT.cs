﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUBEMOVEMENT : MonoBehaviour {

    [SerializeField] float m_movementspeed = 100;

    Rigidbody m_myRigidBody;


	// Use this for initialization
	void Start () {
        m_myRigidBody = gameObject.GetComponent<Rigidbody>();

        if (m_myRigidBody == null )
        {
            Debug.Log("no rigid body found");
        }
	}
	
	// Update is called once per frame
	void Update () {
        float horizontalmovement = Input.GetAxis("Horizontal");
        float verticalmovement = Input.GetAxis("Vertical");
        
        //directional vector for the force
        Vector3 movementforce = new Vector3();

        movementforce.x = horizontalmovement * m_movementspeed * Time.deltaTime;
        movementforce.z = verticalmovement * m_movementspeed * Time.deltaTime;

        m_myRigidBody.AddForce(movementforce,ForceMode.Impulse);
    }
}
