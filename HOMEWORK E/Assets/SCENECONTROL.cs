﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SCENECONTROL : MonoBehaviour {

    [SerializeField] string m_levelToLoad;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}




    private void OnTriggerEnter(Collider other)
    {
        ///invoke calls the function Loadlevel AFTER a second
        Invoke("Loadlevel", 1.0f);

    }

    void Loadlevel()
    {
        SceneManager.LoadScene(m_levelToLoad);
    }
}
